(function ($) {
    $(document).ready(function () {
        setTimeout(function () {
            $('#modal-email-form').modal({ closeExisting: false });
        }, parseInt(config.popup_delay) * 1000);
    });
    $('form.popup-email-form').on('submit', function (e) {
        e.preventDefault();
        var email = $(this).find('.js-email-input').val();

        if (!email || email === '') return false;
        $.post(config.ajaxurl, { ep_email: email, action: 'add_email_to_subs' }, function (data) {
            json = JSON.parse(data);
            if (json.result) {
                $('.popup-email-form').fadeOut(400, function () {
                    $('#modal-email-form .success').fadeIn(200)
                })
            } else {
                $('.popup-email-form').fadeOut(400, function () {
                    $('#modal-email-form .error').fadeIn(200)
                })
            }
        })
    });
})(jQuery);