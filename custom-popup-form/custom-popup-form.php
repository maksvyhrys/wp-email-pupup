<?php
/**
 * Plugin Name: Popup Form
 * Plugin URI: http://localhost/
 * Description: Custom plugin for email form in popup
 * Version: 1.0
 * Author: Maksim Vyhryst
 * Author URI: https://www.upwork.com/o/profiles/users/_~01335b9a64bbd4d600/
 */

define('POPUP_FORM_BASEFILE', __FILE__);
define('META_NAME', 'show-email-popup');

$options_default = array(
    'delay_time' => 10,
    'use_def_css' => true,
    'show_for_logged_in' => false
);

function popup_form_install_db() {
    global $wpdb;
    $prefix_table_name = $wpdb->prefix . 'popup_emails';
    $prefix_db_version = '1.0';
    $prefix_db_installed_version = get_option('popup_email_install_ver');

    if ($prefix_db_installed_version == $prefix_db_version)
        return;

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

    $sql = "CREATE TABLE " . $prefix_table_name . " (
      `id` INT NOT NULL AUTO_INCREMENT,
      `email` VARCHAR(120) NOT NULL UNIQUE,
      `ts` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`)
    );";
    dbDelta($sql);

    update_option('popup_email_install_ver', $prefix_db_version);
}
add_action('plugins_loaded', 'popup_form_install_db');


function register_settings() {
    global $options_default;
    if (!get_option('popup_email_settings')) {
        add_option('popup_email_settings', $options_default, '', 'yes');
    }
}

function inventory_admin_init() {
    if (isset($_REQUEST['page']) && 'popup_email_settings' == $_REQUEST['page']) {
        register_settings();
    }
}
add_action('admin_init', 'inventory_admin_init');

add_action('admin_menu', function () {
    add_menu_page(
        'Popup Emails',
        'Popup Emails',
        'edit_posts',
        'view-emails',
        'view_emails'
    );
    add_options_page(
        'Popup Emails Settings',
        'Popup Emails Settings',
        'manage_options',
        'popup_email_settings',
        'popup_email_settings'
    );
});

function popup_email_settings() {
    if (isset($_POST['config_form_submit']) and
        check_admin_referer(plugin_basename(POPUP_FORM_BASEFILE), 'settings_nonce')) {
        $options_update = array(
            'delay_time' => intval($_POST['delay_time']),
            'use_def_css' => isset($_POST['use_def_css']) ? true : false,
            'show_for_logged_in' => isset($_POST['show_for_logged_in']) ? true : false,
        );
        if (update_option('popup_email_settings', $options_update))
            $message = "Settings was updated!";
        else
            $error = "Unexpected error while saving options. Try again later!";
    }

    include __DIR__ . '/views/admin-settings.php';
}

function meta_box_markup()
{
    global $post;
    wp_nonce_field(basename(__FILE__), 'event_fields');
    $data = get_post_meta($post->ID, META_NAME, true);
    if ($data === '1')
        echo '<input type="checkbox" name="' . META_NAME . '" class="widefat" checked>';
    else
        echo '<input type="checkbox" name="' . META_NAME . '" class="widefat">';
}

function add_popup_meta_box()
{
    add_meta_box(
        META_NAME,
        __("Show email popup?"),
        "meta_box_markup",
        ["post", "page"],
        "side",
        "high",
        null
    );
}

add_action("add_meta_boxes", "add_popup_meta_box");

function save_events_meta($post_id) {
    if (!isset($_POST['event_fields']) ||
        !current_user_can( 'edit_post', $post_id) ||
        !wp_verify_nonce($_POST['event_fields'], basename(__FILE__)))
    {
        return $post_id;
    }
    $current_value = get_post_meta($post_id, META_NAME, true);
    $show_popup = isset($_POST[META_NAME]) ? $_POST[META_NAME] : false;
    if ($current_value || $current_value === '0')
        update_post_meta($post_id, META_NAME, $show_popup ? 1 : 0);
    else
        add_post_meta($post_id, META_NAME, $show_popup ? 1 : 0, true);
}

add_action('save_post', 'save_events_meta', 1);

function has_popup() {
    global $post;
    $is_attached = get_post_meta($post->ID, META_NAME, true) === '1';
    $options = get_option('popup_email_settings');
    return $is_attached && (is_user_logged_in() && $options['show_for_logged_in']);
}

function add_popup() {
    if (has_popup()) include __DIR__ . '/views/popup.php';
}
add_action('wp_footer', 'add_popup');

function register_popup_assets() {
    $options = get_option('popup_email_settings');
    if (!has_popup()) return false;
    if ($options['use_def_css'])
        wp_enqueue_style('popup-styles', plugin_dir_url(__FILE__) . 'assets/css/jquery.modal.min.css');

    wp_enqueue_script('modal-js-lib', plugin_dir_url(__FILE__) . 'assets/js/jquery.modal.min.js', array('jquery'));
    wp_enqueue_script('plugin-js', plugin_dir_url(__FILE__) . 'assets/js/plugin.js', array('jquery', 'modal-js-lib'), true, true);

    wp_localize_script('plugin-js', 'config', [
        'ajaxurl' => admin_url('admin-ajax.php'),
        'popup_delay' => $options['delay_time']
    ]);
}
add_action('wp_enqueue_scripts', 'register_popup_assets');

function email_handler() {
    global $wpdb;
    $table = $wpdb->prefix . 'popup_emails';
    $email = $_POST['ep_email'];
    if (isset($email) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $is_exist = $wpdb->get_var("SELECT email FROM $table where email='$email'");
        if (!$is_exist) $result = $wpdb->insert($table, ['email' => $email]);
        else $result = false;
    }
    echo json_encode(['result' => $result]);
    die();
}
add_action('wp_ajax_add_email_to_subs', 'email_handler');
add_action('wp_ajax_nopriv_add_email_to_subs', 'email_handler');

function view_emails() {
    global $wpdb;
    $prefix_table_name = $wpdb->prefix . 'popup_emails';
    $select_query = "SELECT * FROM $prefix_table_name ORDER BY id DESC";
    $results = $wpdb->get_results($select_query);
    include __DIR__ . '/views/admin-emails.php';
}

