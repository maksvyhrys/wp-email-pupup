<div class="wrap">
    <h1>Popup Settings</h1>
    <div class="updated fade" <?php if (empty($message)) echo "style=\"display:none\""; ?>>
        <p><strong><?php echo $message; ?></strong></p>
    </div>
    <div class="error fade" <?php if (empty($error)) echo "style=\"display:none\""; ?>>
        <p><strong><?php echo $error; ?></strong></p>
    </div>
    <form method="post" class="admin_popup_settings_form" action="">
        <?php $options = get_option('popup_email_settings'); ?>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Popup delay (in seconds):</th>
                <td>
                    <input type="number" name="delay_time" value="<?php echo intval($options['delay_time']); ?>"/>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Use default CSS:</th>
                <td>
                    <input type="checkbox" name="use_def_css" class="widefat" <?php if ($options['use_def_css']): ?>checked<?php endif; ?>>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Show for logged in users:</th>
                <td>
                    <input type="checkbox" name="show_for_logged_in" class="widefat" <?php if ($options['show_for_logged_in']): ?>checked<?php endif; ?>>
                </td>
            </tr>
        </table>
        <p class="submit">
            <input type="submit" id="config_form_submit" class="button-primary" value="Save" />
            <input type="hidden" name="config_form_submit" value="submit" />
            <?php wp_nonce_field(plugin_basename(POPUP_FORM_BASEFILE), 'settings_nonce'); ?>
        </p>
    </form>
</div>
