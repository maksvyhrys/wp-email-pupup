<div class="wrap">
    <h1>Users emails</h1>

    <?php if (!empty($results)) : ?>
        <table class="widefat fixed" cellspacing="0">
            <thead>
            <tr>
                <th>Id</th>
                <th>User email</th>
                <th>Time</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($results as $row): ?>
                <tr>
                    <th><?php echo $row->id ; ?></th>
                    <th><?php echo $row->email ; ?></th>
                    <th><?php echo $row->ts ; ?></th>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <h2>Nothing to show, waiting for emails...</h2>
    <?php endif; ?>
</div>
