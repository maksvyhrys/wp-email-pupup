<div id="modal-email-form" class="modal" style="display: none">
    <h3>Subscribe to our newsletters</h3>
    <form class="popup-email-form">
        <p><input type="email" name="ep_email" class="js-email-input" placeholder="someemail@example.com"></p>
        <p><button type="submit">Share</button></p>
    </form>
    <div class="success" style="display: none">
        <h3>Thank tou for subscription!</h3>
    </div>
    <div class="error" style="display: none">
        <h3>Such email already exist!</h3>
    </div>
</div>